# -*- coding: utf-8 -*-
"""
Created on Mar 1 2019
@author: Leah Krehling
"""

import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import warnings

from math import sqrt
from math import ceil
from scipy import stats
from sklearn.linear_model import Lasso
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import KFold
from sklearn.metrics import mean_squared_error
from sklearn.metrics import r2_score

warnings.filterwarnings("ignore")

############## methods for exploring the data ##############
def describe(df, pred=None): 
    obs = df.shape[0]
    types = df.dtypes
    counts = df.apply(lambda x: x.count())
    uniques = df.apply(lambda x: [x.unique()])
    nulls = df.apply(lambda x: x.isnull().sum())
    distincts = df.apply(lambda x: x.unique().shape[0])
    missing_ration = (df.isnull().sum()/ obs) * 100
    skewness = df.skew()
    kurtosis = df.kurt() 
    print 'Data shape:', df.shape 
    
    if pred is None:
        cols = ['types', 'counts', 'distincts', 'nulls', 'missing ration', 'uniques', 'skewness', 'kurtosis']
        str = pd.concat([types, counts, distincts, nulls, missing_ration, uniques, skewness, kurtosis], axis = 1)

    else:
        corr = df.corr()[pred]
        str = pd.concat([types, counts, distincts, nulls, missing_ration, uniques, skewness, kurtosis, corr], axis = 1, sort=False)
        corr_col = 'corr '  + pred
        cols = ['types', 'counts', 'distincts', 'nulls', 'missing_ration', 'uniques', 'skewness', 'kurtosis', corr_col ]
    
    str.columns = cols
    dtypes = str.types.value_counts()
    print "___________________________\nData types:\n",dtypes
    print "___________________________"
    return str

# Split into pieces       
def chunks(l, n):
    return [l[i:i + n] for i in range(0, len(l), n)]

#displaying the skew of data
def distplot(data, cols, ncols):
    nrows = (int)(ceil(len(cols) / ncols))
    fig, axes = plt.subplots(nrows = nrows, ncols = ncols, figsize = (8, 6), dpi = 300)
    for index in range(0, len(cols)):
        x = (int)(index / ncols)
        y = (int)(index % ncols)
        attr = cols[index]
        sns.distplot(data[attr], fit=stats.norm, ax=axes.item((x, y)))
    plt.tight_layout()

############## methods for processing the data ##############
# impute missing values
def impute(data, cols):
    for col in cols:
        data[col] = data[col].fillna(data[col].mean())

# Cleaning outliers
def clean_outliers(dataTable, outliers):
    for col in outliers:
        dataTable = dataTable[dataTable[col] < outliers[col]]
    return dataTable

#Discretizing categorical data
def discretize(data, cols, qualityTerms, qualityColumns):
    for col in cols:
        data[col] = data[col].fillna("NONE")
        if col in qualityColumns:
            data[col] = data[col].map(qualityTerms).astype('int')
        else:
            data[col] = data[col].astype('category')
    cat_columns = data.select_dtypes(['category']).columns
    data[cat_columns] = data[cat_columns].apply(lambda x: x.cat.codes)

############## methods for lasso regression ##############
def evaluate(model, X, y, k=10):
    kf = KFold(n_splits=k, shuffle=True, random_state=2)
    y_pr = np.zeros((X.shape[0],))
    for train_index, test_index in kf.split(X):
        X_tr, X_te = X.iloc[train_index], X.iloc[test_index]
        y_tr, y_te = y.iloc[train_index], y.iloc[test_index]
        model.fit(X_tr, y_tr)
        y_pr[test_index] = model.predict(X_te)
    RMSerr = sqrt(mean_squared_error(y_pr, y))
    print("Errors for training set: \nRMSE: %.5f" % RMSerr)
    print "R^2 is: ", model.score(X_te, y_te) #Returns the mean accuracy (for classifiers) or R² score (for regression estimators)
    print "R^2 is: ", r2_score(y, y_pr) #to test the truth of model.score giving the R^2 values 
    
def coef(model, X):
    for l, r in sorted(zip(X.columns, model.coef_), key=lambda x: abs(x[1]), reverse=True)[:5]:
        print("(%s, %.5f)" % (l, r))

def print_params(est, X, y, n=100):
    print("Chosen parameter on %d datapoints: %s" % (n,est.fit(X[:n], y[:n]).best_params_))

# Create the predictions
def generate_submission(model, X):
    y_pr = model.predict(X)
    result = np.exp(y_pr) 
    submission = pd.DataFrame({'Id': X.index.values, 'SalePrice': result})
    submission.to_csv("submission_Lasso.csv", index=None)



################## Main Method ##################
def main():
    ############## importing the data ##############
    trainingData = pd.read_csv('train.csv', index_col = "Id")
    testingData = pd.read_csv('test.csv', index_col = "Id")
    
    ############## Exploring the Data ##############
    details = describe(trainingData, 'SalePrice')
    print(details.sort_values(by='corr SalePrice', ascending=False))
    # Looking at the numerical Data, the count shows that values are missing, 
    print(trainingData.describe().transpose())
    
    describe(testingData)
     
    #The numerical data is broken into two types
    # 14 Discrete
    discrete = ["YearBuilt", "YearRemodAdd", "BsmtFullBath", "BsmtHalfBath", "FullBath", "HalfBath", "BedroomAbvGr", "KitchenAbvGr",
                "TotRmsAbvGrd", "Fireplaces", "GarageYrBlt", "GarageCars", "MoSold", "YrSold"]
    # 20 continuous
    continuous = ["LotFrontage", "LotArea", "MasVnrArea", "BsmtFinSF1", "BsmtFinSF2", "BsmtUnfSF", "TotalBsmtSF", "1stFlrSF", 
                  "2ndFlrSF", "LowQualFinSF", "GrLivArea", "GarageArea", "WoodDeckSF", "OpenPorchSF", "EnclosedPorch", "3SsnPorch", 
                  "ScreenPorch", "PoolArea", "MiscVal", "SalePrice"]
    numerical = discrete + continuous
    numerical.remove("SalePrice")
    
    # Visualising the numerical data
    numericalData = pd.concat([trainingData['SalePrice'], trainingData[numerical]], axis=1)
    for lst in chunks(numerical, 5):
        sns.pairplot(numericalData, y_vars=['SalePrice'], x_vars=lst)
    
    ############## Data preprocessing ##############
    
    # finding the missing values in the numerical columns
    missing = []
    for col in numerical:
        count = trainingData[col].isnull().sum() + testingData[col].isnull().sum()
        if count:
            missing.append(col)
    # impute the missing values with the mean value of the column
    impute(trainingData, missing)
    impute(testingData, missing)
    
    #Dealing with outliers
    outliers = {"LotArea": 150000, "BsmtFinSF1": 4000, "TotalBsmtSF": 6000, "1stFlrSF": 4000, "GrLivArea": 5000}
    
    print("Before cleaning: %d" % len(trainingData))
    trainingData = clean_outliers(trainingData, outliers)
    print("After cleaning: %d" % len(trainingData)) #4 rows with outliers were removed
    
    #Discretizing the categorical data
    # categorical attributes
    categorical = ["MSZoning", "Street", "Alley", "LotShape", "LandContour", "Utilities", "LotConfig", "LandSlope", "Neighborhood", 
                   "Condition1", "Condition2", "BldgType", "HouseStyle", "RoofStyle", "RoofMatl", "Exterior1st", "Exterior2nd", 
                   "MasVnrType", "ExterQual", "ExterCond", "Foundation", "BsmtQual", "BsmtCond", "BsmtExposure", "BsmtFinType1", 
                   "BsmtFinType2", "Heating", "HeatingQC", "CentralAir", "Electrical", "KitchenQual", "Functional", "FireplaceQu", 
                   "GarageType", "GarageFinish", "GarageQual", "GarageCond", "PavedDrive", "PoolQC", "Fence", "MiscFeature", "SaleType", "SaleCondition"]
    
    quality_dict = {"NONE": 0, "Po": 1, "Fa": 2, "TA": 4, "Gd": 7, "Ex": 11} #dictonary to covert to discrete values
    quality_cols = ["ExterQual", "ExterCond", "BsmtQual", "BsmtCond", "HeatingQC", "KitchenQual", "FireplaceQu", "GarageQual", "GarageCond", "PoolQC"]
    
    dataSet = pd.concat(objs=[trainingData, testingData], axis=0, sort=True)
    discretize(dataSet, categorical, quality_dict, quality_cols)
    
    idx = len(trainingData)
    trainingData = dataSet[:idx]
    testingData = dataSet[idx:]
    
    #Transforming skewed attributes    
    numeric = discrete + continuous
    dataSkew = trainingData[numeric].apply(lambda x: stats.skew(x.astype('float')))
    dataSkew = dataSkew[abs(dataSkew) > 0.75]
    
    # before transformation
    distplot(trainingData, dataSkew.index, 4)
    
    for col in dataSkew.index:
        trainingData[col] = np.log1p(trainingData[col])
        testingData[col] = np.log1p(testingData[col])
    
    # after transformation
    distplot(trainingData, dataSkew.index, 4)
    
    
    ############## Lasso Regression ##############
    ## Setting up values
    X_training = trainingData.drop("SalePrice", axis=1)
    y_training = trainingData["SalePrice"]
    X_testing = testingData.drop("SalePrice", axis=1)
    
    params = {'max_iter': 50000}
    lasso = Lasso(**params)
    est = GridSearchCV(lasso, param_grid={"alpha": np.arange(0.0005, 0.001, 0.00001)}) # testing values from 0.00005 to 0.001
    print_params(est, X_training, y_training, 100)

    params = {'alpha': 0.00099, 'max_iter': 50000}
    lasso = Lasso(**params)

    # cross validation
    evaluate(lasso, X_training, y_training, 10)

    # top 5 attributes from training set
    coef(lasso, X_training)

    # generate predictions using created lasso model and testing data
    generate_submission(lasso, X_testing)


if __name__ == '__main__':
    main()
